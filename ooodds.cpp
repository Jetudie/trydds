#include "ooodds.h"

#include <QString>
#include <QObject>
#include <dds/pub/ddspub.hpp>
#include <rti/util/util.hpp> // for sleep()
#include "Example.hpp"

oooDDS::oooDDS(int domain, DeviceData *Data):
    Data(Data),
    dds_participant(new dds::domain::DomainParticipant(domain)),
    dds_topic(new dds::topic::Topic<Example>(*this->dds_participant, "Example Example")),
    dds_publisher(new dds::pub::Publisher(*this->dds_participant)),
    dds_writer(new dds::pub::DataWriter<Example>(*this->dds_publisher, *this->dds_topic)),
    dds_sample(new Example)
{
    //this->participant = new dds::domain::DomainParticipant(0);
    //this->topic = new dds::topic::Topic<Example>(*this->participant, "Example Example");
    //this->writer = new dds::pub::DataWriter<Example>(dds::pub::Publisher(*this->participant), *this->topic);
}

void oooDDS::dds_write()
{ 
    emit response_pub("PUBLISHING");
    this->running = 1;
    while(this->count){
        this->dds_sample->device(this->Data->device);
        this->dds_sample->value(this->Data->value);
        this->dds_writer->write(*this->dds_sample);
        this->count -= this->always;
        sleep(this->delaytime);
    }
    this->running = 0;
    emit response_pub("NO PUB");
}

void oooDDS::dds_destroy()
{
    if(this->running){
        set_count(0);
        sleep(this->delaytime+1);
    }
    delete this->dds_participant;
    delete this->dds_topic;
    delete this->dds_publisher;
    delete this->dds_writer;
    delete this->dds_sample;
}
