#-------------------------------------------------
#
# Project created by QtCreator 2018-10-19T09:01:08
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = TryDDS
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
        main.cpp \
        mainwindow_dds.cpp \
        Example.cxx \
        ExampleImpl.cxx \
        ExampleImplPlugin.cxx \
        ooodds.cpp
        #Example_publisher.cxx \
        #Example_subscriber.cxx
        #USER_QOS_PROFILES.xml
        #Example.idl

HEADERS += \
        mainwindow_dds.h \
        ExampleImplPlugin.h \
        Example.hpp \
        ExampleImpl.h \
        ooodds.h

FORMS += \
        mainwindowdds.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

#CDRSOURCES = ooEOS.idl
#LINKER_FLAGS = -m64 -Wl,--no-as-needed
NDDSHOME = /home/iclab/work/rti_connext_dds-5.3.1
TARGET_ARCH = x64Linux3gcc5.4.0
SYSLIBS = -ldl -lnsl -lm -lpthread -lrt
#LIBS += -L$(NDDSHOME)/lib/$(TARGET_ARCH) \
#        -lnddsc$(SHAREDLIB_SFX)$(DEBUG_SFX) -lnddscore$(SHAREDLIB_SFX)$(DEBUG_SFX) $(SYSLIBS)
#INCLUDES = -I. -I$(NDDSHOME)/include -I$(NDDSHOME)/include/ndds -I$(NDDSHOME)/include/ndds/hpp

#Add lib for dds
DEFINES += RTI_UNIX RTI_LINUX RTI_64BIT RTI_STATIC
LIBS += -L$$NDDSHOME/lib/$$TARGET_ARCH \
        -lnddscpp2z -lnddscz -lnddscorez $$SYSLIBS

INCLUDEPATH += $$NDDSHOME/lib/x64Linux3gcc5.4.0
DEPENDPATH += $$NDDSHOME/lib/x64Linux3gcc5.4.0
INCLUDEPATH += \
    $$NDDSHOME/include \
    $$NDDSHOME/include/ndds \
    $$NDDSHOME/include/ndds/hpp
