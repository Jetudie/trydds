

/*
WARNING: THIS FILE IS AUTO-GENERATED. DO NOT MODIFY.

This file was generated from Example.idl using "rtiddsgen".
The rtiddsgen tool is part of the RTI Connext distribution.
For more information, type 'rtiddsgen -help' at a command shell
or consult the RTI Connext manual.
*/

#ifndef Example_276139054_hpp
#define Example_276139054_hpp

#include <iosfwd>
#include "ExampleImpl.h"

#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, start exporting symbols.
*/
#undef RTIUSERDllExport
#define RTIUSERDllExport __declspec(dllexport)
#endif

#include "dds/domain/DomainParticipant.hpp"
#include "dds/topic/TopicTraits.hpp"
#include "dds/core/SafeEnumeration.hpp"
#include "dds/core/String.hpp"
#include "dds/core/array.hpp"
#include "dds/core/vector.hpp"
#include "dds/core/Optional.hpp"
#include "dds/core/xtypes/DynamicType.hpp"
#include "dds/core/xtypes/StructType.hpp"
#include "dds/core/xtypes/UnionType.hpp"
#include "dds/core/xtypes/EnumType.hpp"
#include "dds/core/xtypes/AliasType.hpp"
#include "rti/core/array.hpp"
#include "rti/util/StreamFlagSaver.hpp"
#include "rti/domain/PluginSupport.hpp"
#include "rti/core/LongDouble.hpp"
#include "rti/core/Pointer.hpp"
#include "rti/topic/TopicTraits.hpp"
#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, stop exporting symbols.
*/
#undef RTIUSERDllExport
#define RTIUSERDllExport
#endif

#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, start exporting symbols.
*/
#undef NDDSUSERDllExport
#define NDDSUSERDllExport __declspec(dllexport)
#endif

class NDDSUSERDllExport Example {

  public:
    Example();

    Example(
        const dds::core::string& device_param,
        int32_t value_param);

    #ifdef RTI_CXX11_RVALUE_REFERENCES
    #ifndef RTI_CXX11_NO_IMPLICIT_MOVE_OPERATIONS
    Example (Example&&) = default;
    Example& operator=(Example&&) = default;
    Example& operator=(const Example&) = default;
    Example(const Example&) = default;
    #else
    Example(Example&& other_) OMG_NOEXCEPT;  
    Example& operator=(Example&&  other_) OMG_NOEXCEPT;
    #endif
    #endif 

    dds::core::string& device() OMG_NOEXCEPT; 
    const dds::core::string& device() const OMG_NOEXCEPT;
    void device(const dds::core::string& value);

    int32_t value() const OMG_NOEXCEPT;
    void value(int32_t value);

    bool operator == (const Example& other_) const;
    bool operator != (const Example& other_) const;

    void swap(Example& other_) OMG_NOEXCEPT;

  private:

    dds::core::string m_device_;
    int32_t m_value_;

};

inline void swap(Example& a, Example& b)  OMG_NOEXCEPT 
{
    a.swap(b);
}

NDDSUSERDllExport std::ostream& operator<<(std::ostream& o,const Example& sample);

namespace dds { 
    namespace topic {

        template<>
        struct topic_type_name<Example> {
            NDDSUSERDllExport static std::string value() {
                return "Example";
            }
        };

        template<>
        struct is_topic_type<Example> : public dds::core::true_type {};

        template<>
        struct topic_type_support<Example> {

            NDDSUSERDllExport static void initialize_sample(Example& sample);

            NDDSUSERDllExport static void register_type(
                dds::domain::DomainParticipant& participant,
                const std::string & type_name);

            NDDSUSERDllExport static std::vector<char>& to_cdr_buffer(
                std::vector<char>& buffer, const Example& sample);

            NDDSUSERDllExport static void from_cdr_buffer(Example& sample, const std::vector<char>& buffer);

            static const rti::topic::TypePluginKind::type type_plugin_kind = 
            rti::topic::TypePluginKind::NON_STL;
        };

    }
}

namespace rti { 
    namespace topic {
        template<>
        struct dynamic_type<Example> {
            typedef dds::core::xtypes::StructType type;
            NDDSUSERDllExport static const dds::core::xtypes::StructType& get();
        };

        template<>
        struct impl_type<Example> {
            typedef Example_c type;
        };

    }
}

#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, stop exporting symbols.
*/
#undef NDDSUSERDllExport
#define NDDSUSERDllExport
#endif

#endif // Example_276139054_hpp

