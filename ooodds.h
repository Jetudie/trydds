#ifndef OOODDS_H
#define OOODDS_H

#include <QThread>
#include <QString>
#include <QObject>
#include <dds/pub/ddspub.hpp>
#include <rti/util/util.hpp> // for sleep()
#include "Example.hpp"

typedef struct DeviceData{
    std::string device;
    int value;
}DeviceData;

class oooDDS: public QThread
{
    Q_OBJECT
public:
    explicit oooDDS(int domain, DeviceData *Data);
    void set_count(int n){ this->count = std::abs(n);}
    void set_delay(int t){ this->delaytime = std::abs(t);}
    void set_always(int n){ this->always = (n)? 0:1;}
    void dds_write();
    void dds_destroy();
    void run () {
        this->dds_write();
    }

private:
    int count;
    int always;
    int running;
    unsigned int delaytime;
    DeviceData *Data;
    dds::domain::DomainParticipant *dds_participant;
    dds::topic::Topic<Example> *dds_topic;
    dds::pub::Publisher *dds_publisher;
    dds::pub::DataWriter<Example> *dds_writer;
    Example *dds_sample;

signals:
    void response_pub(QString str1);
};

#endif
// OOODDS_H
