

/*
WARNING: THIS FILE IS AUTO-GENERATED. DO NOT MODIFY.

This file was generated from ExampleImpl.idl using "rtiddsgen".
The rtiddsgen tool is part of the RTI Connext distribution.
For more information, type 'rtiddsgen -help' at a command shell
or consult the RTI Connext manual.
*/

#ifndef ExampleImpl_276139054_h
#define ExampleImpl_276139054_h

#ifndef NDDS_STANDALONE_TYPE
#ifndef ndds_c_h
#include "ndds/ndds_c.h"
#endif
#else
#include "ndds_standalone_type.h"
#endif

extern const char *Example_cTYPENAME;

typedef struct Example_c {

    DDS_Char *   device ;
    DDS_Long   value ;

    Example_c() {}

} Example_c ;
#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, start exporting symbols.
*/
#undef NDDSUSERDllExport
#define NDDSUSERDllExport __declspec(dllexport)
#endif

NDDSUSERDllExport DDS_TypeCode* Example_c_get_typecode(void); /* Type code */

DDS_SEQUENCE(Example_cSeq, Example_c);

NDDSUSERDllExport
RTIBool Example_c_initialize(
    Example_c* self);

NDDSUSERDllExport
RTIBool Example_c_initialize_ex(
    Example_c* self,RTIBool allocatePointers,RTIBool allocateMemory);

NDDSUSERDllExport
RTIBool Example_c_initialize_w_params(
    Example_c* self,
    const struct DDS_TypeAllocationParams_t * allocParams);  

NDDSUSERDllExport
void Example_c_finalize(
    Example_c* self);

NDDSUSERDllExport
void Example_c_finalize_ex(
    Example_c* self,RTIBool deletePointers);

NDDSUSERDllExport
void Example_c_finalize_w_params(
    Example_c* self,
    const struct DDS_TypeDeallocationParams_t * deallocParams);

NDDSUSERDllExport
void Example_c_finalize_optional_members(
    Example_c* self, RTIBool deletePointers);  

NDDSUSERDllExport
RTIBool Example_c_copy(
    Example_c* dst,
    const Example_c* src);

#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, stop exporting symbols.
*/
#undef NDDSUSERDllExport
#define NDDSUSERDllExport
#endif

#endif /* ExampleImpl */

