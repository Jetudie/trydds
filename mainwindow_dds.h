#ifndef MAINWINDOW_DDS_H
#define MAINWINDOW_DDS_H

#include <QMainWindow>

#include <dds/pub/ddspub.hpp>
#include <rti/util/util.hpp> // for sleep()
#include "Example.hpp"
#include "ooodds.h"

namespace Ui {
class MainWindowDDS;
}

class MainWindowDDS : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindowDDS(QWidget *parent = nullptr);
    ~MainWindowDDS();
    //void dds_publisher(int domain_id, int sample_count);

private slots:
    void on_HSlider1_valueChanged(int value);
    void on_checkBox_pub_stateChanged(int arg1);

private:
    DeviceData Data;
    oooDDS *DDS;
    Ui::MainWindowDDS *ui;
};

#endif // MAINWINDOW_DDS_H
