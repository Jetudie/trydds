

/*
WARNING: THIS FILE IS AUTO-GENERATED. DO NOT MODIFY.

This file was generated from Example.idl using "rtiddsgen".
The rtiddsgen tool is part of the RTI Connext distribution.
For more information, type 'rtiddsgen -help' at a command shell
or consult the RTI Connext manual.
*/

#include <iosfwd>
#include <iomanip>
#include "Example.hpp"
#include "ExampleImplPlugin.h"

#include <rti/util/ostream_operators.hpp>

// ---- Example: 

Example::Example() :
    m_value_ (0) {
}   

Example::Example (
    const dds::core::string& device_param,
    int32_t value_param)
    :
        m_device_( device_param ),
        m_value_( value_param ) {
}
#ifdef RTI_CXX11_RVALUE_REFERENCES
#ifdef RTI_CXX11_NO_IMPLICIT_MOVE_OPERATIONS
Example::Example(Example&& other_) OMG_NOEXCEPT  :m_device_ (std::move(other_.m_device_))
,
m_value_ (std::move(other_.m_value_))
{
} 

Example& Example::operator=(Example&&  other_) OMG_NOEXCEPT {
    Example tmp(std::move(other_));
    swap(tmp); 
    return *this;
}
#endif
#endif   

void Example::swap(Example& other_)  OMG_NOEXCEPT 
{
    using std::swap;
    swap(m_device_, other_.m_device_);
    swap(m_value_, other_.m_value_);
}  

bool Example::operator == (const Example& other_) const {
    if (m_device_ != other_.m_device_) {
        return false;
    }
    if (m_value_ != other_.m_value_) {
        return false;
    }
    return true;
}
bool Example::operator != (const Example& other_) const {
    return !this->operator ==(other_);
}

// --- Getters and Setters: -------------------------------------------------
dds::core::string& Example::device() OMG_NOEXCEPT {
    return m_device_;
}

const dds::core::string& Example::device() const OMG_NOEXCEPT {
    return m_device_;
}

void Example::device(const dds::core::string& value) {
    m_device_ = value;
}

int32_t Example::value() const OMG_NOEXCEPT{
    return m_value_;
}

void Example::value(int32_t value) {
    m_value_ = value;
}

std::ostream& operator << (std::ostream& o,const Example& sample)
{
    rti::util::StreamFlagSaver flag_saver (o);
    o <<"[";
    o << "device: " << sample.device()<<", ";
    o << "value: " << sample.value() ;
    o <<"]";
    return o;
}

// --- Type traits: -------------------------------------------------

namespace rti { 
    namespace topic {

        const dds::core::xtypes::StructType& dynamic_type<Example>::get()
        {
            return static_cast<const dds::core::xtypes::StructType&>(
                rti::core::native_conversions::cast_from_native<dds::core::xtypes::DynamicType>(
                    *(Example_c_get_typecode())));
        }

    }
}  

namespace dds { 
    namespace topic {
        void topic_type_support<Example>:: register_type(
            dds::domain::DomainParticipant& participant,
            const std::string& type_name){

            rti::domain::register_type_plugin(
                participant,
                type_name,
                Example_cPlugin_new,
                Example_cPlugin_delete);
        }

        void topic_type_support<Example>::initialize_sample(Example& sample){

            Example_c* native_sample=reinterpret_cast<Example_c*> (&sample);

            struct DDS_TypeDeallocationParams_t deAllocParams = {RTI_FALSE, RTI_FALSE};
            Example_c_finalize_w_params(native_sample,&deAllocParams);

            struct DDS_TypeAllocationParams_t allocParams = {RTI_FALSE, RTI_FALSE, RTI_TRUE}; 
            RTIBool ok=Example_c_initialize_w_params(native_sample,&allocParams);
            rti::core::check_return_code(
                ok ? DDS_RETCODE_OK : DDS_RETCODE_ERROR,
                "Failed to initialize_w_params");

        } 

        std::vector<char>& topic_type_support<Example>::to_cdr_buffer(
            std::vector<char>& buffer, const Example& sample)
        {
            // First get the length of the buffer
            unsigned int length = 0;
            RTIBool ok = Example_cPlugin_serialize_to_cdr_buffer(
                NULL, &length,reinterpret_cast<const Example_c*>(&sample));
            rti::core::check_return_code(
                ok ? DDS_RETCODE_OK : DDS_RETCODE_ERROR,
                "Failed to calculate cdr buffer size");

            // Create a vector with that size and copy the cdr buffer into it
            buffer.resize(length);
            ok = Example_cPlugin_serialize_to_cdr_buffer(
                &buffer[0], &length, reinterpret_cast<const Example_c*>(&sample));
            rti::core::check_return_code(
                ok ? DDS_RETCODE_OK : DDS_RETCODE_ERROR,
                "Failed to copy cdr buffer");

            return buffer;

        }

        void topic_type_support<Example>::from_cdr_buffer(Example& sample, 
        const std::vector<char>& buffer)
        {

            RTIBool ok  = Example_cPlugin_deserialize_from_cdr_buffer(
                reinterpret_cast<Example_c*> (&sample), &buffer[0], 
                static_cast<unsigned int>(buffer.size()));
            rti::core::check_return_code(
                ok ? DDS_RETCODE_OK : DDS_RETCODE_ERROR,
                "Failed to create Example from cdr buffer");
        }

    }
}  

