#include "mainwindow_dds.h"
#include <QApplication>

#include <dds/pub/ddspub.hpp>
#include <rti/util/util.hpp> // for sleep()
#include "Example.hpp"

int main(int argc, char *argv[])
{
    // Create a DomainParticipant with default Qos
    QApplication a(argc, argv);
    MainWindowDDS w;
    w.show();
    return a.exec();
}
