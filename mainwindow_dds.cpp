#include "mainwindow_dds.h"
#include "ui_mainwindowdds.h"

#include <QObject>
#include <iostream>
#include <dds/pub/ddspub.hpp>
#include <rti/util/util.hpp> // for sleep()
#include "Example.hpp"
#include "ooodds.h"

MainWindowDDS::MainWindowDDS(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindowDDS)
{
    ui->setupUi(this);
    this->Data.device = "hahaha";
    //DDS = new oooDDS(0, &this->Data);
}

MainWindowDDS::~MainWindowDDS()
{
    delete ui;
}

void MainWindowDDS::on_HSlider1_valueChanged(int value)
{
    ui->label_value->setNum(value);
    this->Data.value = ui->label_value->text().toInt();
}


void MainWindowDDS::on_checkBox_pub_stateChanged(int arg1)
{
    if(ui->checkBox_pub->isChecked())
    {
        // create a DDS if no one exists
        if(this->DDS == nullptr)
            this->DDS = new oooDDS(0, &this->Data);

        //set the label_pub to published
        connect(this->DDS, SIGNAL(response_pub(QString)), this->ui->label_pub, SLOT(setText(QString)));
        DDS->set_count(ui->spinBoxc->value());
        DDS->set_delay(ui->spinBoxt->value());
        DDS->set_always(ui->checkBox_pub_always->checkState());
        DDS->start();
    }
    else{
        // Destroy participants, topic, writer and instance in DDS
        DDS->dds_destroy();
        // Free DDS
        delete this->DDS;
        this->DDS = nullptr;
    }
}
